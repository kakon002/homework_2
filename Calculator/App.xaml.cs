﻿using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace Calculator
{
    public partial class App : Application
    {
        public App()
        {
            Debug.WriteLine($"**** {this.GetType().Name} .{nameof(App)} :  constructor");
            InitializeComponent();

            MainPage = new CalculatorPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            Debug.WriteLine($"**** {this.GetType().Name} .{nameof(OnStart)} ");
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Debug.WriteLine($"**** {this.GetType().Name} .{nameof(OnSleep)} ");

        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            Debug.WriteLine($"**** {this.GetType().Name} .{nameof(OnResume)} ");
        }
    }
}
