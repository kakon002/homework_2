﻿using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Calculator
{
    public partial class CalculatorPage : ContentPage
    {
        int counter = 1;
        string symbol;
        double FirstNum, SecondNum;

        public CalculatorPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name} .{nameof(CalculatorPage)} :  constructor");
            InitializeComponent();

            Clear(this, null);
        }
        //-----------------------------------------------------------------------------------------------
        void NumberClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string pressed = button.Text;

            if(CalculatorLabel.Text == "0" || counter < -1)
            {
                this.CalculatorLabel.Text = "";
                if (counter < -1)
                    counter *= -1;
            }

            this.CalculatorLabel.Text += pressed; //concatinating the numbers ex:12345

            double number;
            if(double.TryParse(this.CalculatorLabel.Text, out number))
            {
                this.CalculatorLabel.Text = number.ToString("N0");
                if(counter == 1)
                {
                    FirstNum = number;
                }
                else
                {
                    SecondNum = number;
                }
            }

        }
        //-----------------------------------------------------------------------------------------------
        void Clear( object sender, EventArgs e)
        {
            FirstNum = 0;
            SecondNum = 0;
            counter = 1;
            this.CalculatorLabel.Text = "0";
        }
        //-----------------------------------------------------------------------------------------------
        void Symbol(object sender, EventArgs e)
        {
            counter = -2;
            Button button = (Button)sender;
            string pressed = button.Text;
            symbol = pressed;
        }
        //-----------------------------------------------------------------------------------------------
        void Calculations(object sender, EventArgs e)
        {
            if(counter == 2)
            {
                var result = Calculate(FirstNum, SecondNum, symbol);

                this.CalculatorLabel.Text = result.ToString("N0");
                FirstNum = result;
                counter = -1;
            }
        }
        //-----------------------------------------------------------------------------------------------
        double Calculate(double Num1, double Num2, string sym)
        {
            double result = 0;

            switch(sym)
            {
                case "÷": result = Num1 / Num2; 
                    break;
                case "x": result = Num1 * Num2;
                    break;
                case "+": result = Num1 + Num2;
                    break;
                case "-": result = Num1 - Num2; 
                    break;    
            }
            return result;
        }
    }
}
